//
//  GalleryViewModel.swift
//  Gallery
//
//  Created by Tony Thomas on 17/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit

final class GalleryViewModel {
    
    
    lazy var itemsList: [[GalleryItem]]? = nil
    
    
    
    func populateItemList(completion: @escaping ()->Void)  {
        
        //Populate the data in worker thread
        DispatchQueue.global().async {
            // keep the number of rows hardcorded
            let kNumberOfRows = 10
            self.itemsList = [[GalleryItem]]()
            for _ in  0..<kNumberOfRows {
                // keep the number of columns random between 10 and 99
                var channel = [GalleryItem]()
                let numberOfColumns = 10 + arc4random_uniform(90)
                for j in 0..<numberOfColumns {
                    let galleyItem = GalleryItem(title: "Asset \(j)", imageUrl: self.getNextImageUrl(), image: nil)
                    channel.append(galleyItem)
                }
                self.itemsList?.append(channel)
            }
            //Switch to main thread for UI update
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func getNextImageUrl() -> String {
        let random =  500000 + arc4random_uniform(999999-500000)
        return "http://www.colourlovers.com/img/\(random)/300/200/watery.png"
    }
    


}

