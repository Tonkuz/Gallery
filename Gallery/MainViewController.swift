//
//  ViewController.swift
//  Gallery
//
//  Created by Tony Thomas on 15/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.register(MainTableViewCell.self, forCellReuseIdentifier: self.kTablevieCellResuseID)
        tv.register(TableViewHeader.self, forHeaderFooterViewReuseIdentifier: self.kTablevieHeaderResuseID)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.dataSource = self
        tv.delegate = self
        tv.separatorStyle = .none
        tv.allowsSelection = false
        return tv
    }()
    
    
    lazy var viewModel = GalleryViewModel()
    let kTablevieCellResuseID = "kTablevieCellResuseID"
    let kTablevieHeaderResuseID = "kTablevieHeaderResuseID"
    let kCollectionViewCellReuseUD = "kCollectionViewCellReuseUD"
    var animationInProgress: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "Gallery"
        view.addSubview(tableView)
        doLayout()
        
        viewModel.populateItemList {
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doLayout() {
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tv]-0-|", options: [], metrics: nil, views: ["tv": tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[tv]-0-|", options: [], metrics: nil, views: ["tv": tableView]))
    }
    
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kTablevieCellResuseID, for: indexPath) as! MainTableViewCell
        cell.collectionView.dataSource = self
        cell.collectionView.delegate = self
        cell.collectionView.tag = indexPath.section
        cell.collectionView.collectionViewLayout.invalidateLayout()
        cell.collectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.itemsList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            return 200
        case 3:
            return 125
        case 5:
            return 200
        default:
            return 150
        }
    }
    
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headeView = tableView.dequeueReusableHeaderFooterView(withIdentifier: kTablevieHeaderResuseID) as! TableViewHeader
        headeView.title.text = "Channel \(section)"
        return headeView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
}

extension MainViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCollectionViewCellReuseUD, for:  indexPath) as! CollectionViewCell
        let item = viewModel.itemsList?[collectionView.tag][indexPath.row]
        cell.setItem(item: item, indexPath: indexPath, imageDownloadHandler: { newImage in
            if let newImage = newImage , let item = item{
                self.viewModel.itemsList![collectionView.tag][indexPath.row] = GalleryItem(item: item, image: newImage)
            }
            collectionView.reloadItems(at: [indexPath])
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.itemsList?[collectionView.tag].count ?? 0
    }
}

extension MainViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = viewModel.itemsList?[collectionView.tag][indexPath.row]
        let detailVC = DetailsViewController()
        detailVC.imageView.image = item?.image ?? UIImageView.placeHolder
        detailVC.title = item?.title
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag {
        case 1:
            return CGSize(width: 300, height: 200)
        case 3:
            return CGSize(width: 175, height: 125)
        case 5:
            return CGSize(width: 200, height: 200)
        default:
            return CGSize(width: 200, height: 150)
        }
    }
}





