//
//  ChannelCollectionViewCell.swift
//  Gallery
//
//  Created by Tony Thomas on 17/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleToFill
        iv.backgroundColor = UIColor.black
        return iv
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.lightText
        return label
    }()
    
    lazy var infoImageView:UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        doLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func doLayout() {
        let horizontalStackView = UIStackView(arrangedSubviews: [titleLabel, infoImageView])
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .fill
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        
        infoImageView.addConstraint(NSLayoutConstraint(item: infoImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 30))
        horizontalStackView.addConstraint(NSLayoutConstraint(item: horizontalStackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 30))
        
        let verticalStalkView = UIStackView(arrangedSubviews: [imageView, horizontalStackView])
        verticalStalkView.axis = .vertical
        verticalStalkView.distribution = .equalSpacing
        verticalStalkView.spacing = 5
        verticalStalkView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(verticalStalkView)
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[vsv]-0-|", options: [], metrics: nil, views: ["vsv": verticalStalkView]))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[vsv]-0-|", options: [], metrics: nil, views: ["vsv": verticalStalkView]))
    }
    
    func setItem(item: GalleryItem?, indexPath: IndexPath, imageDownloadHandler: @escaping (UIImage?)->Void) {
        infoImageView.setInfoImage()
        guard let item = item else {
            imageView.setItemPlaceHolder()
            titleLabel.text = nil
            return
        }
        if let image = item.image {
            imageView.image = image
        } else {
            imageView.setItemPlaceHolder()
            ImageDownloader.shared.downloadImage(url: item.imageUrl, completion: { (image) in
                DispatchQueue.main.async {
                    imageDownloadHandler(image)
                }
            })
        }
        titleLabel.text = item.title
    }

}
