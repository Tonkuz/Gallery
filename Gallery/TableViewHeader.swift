//
//  TableViewHeader.swift
//  Gallery
//
//  Created by Tony Thomas on 17/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit

class TableViewHeader: UITableViewHeaderFooterView {

    lazy var title: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = UIColor.lightText
        return label
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.addSubview(title)
        contentView.backgroundColor = UIColor.black

        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tv]-0-|", options: [], metrics: nil, views: ["tv": title]))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[tv]-0-|", options: [], metrics: nil, views: ["tv": title]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
