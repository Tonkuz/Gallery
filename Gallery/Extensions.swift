//
//  Extensions.swift
//  Gallery
//
//  Created by Tony Thomas on 17/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit

extension UIImageView {
    
    static let placeHolder: UIImage = UIImage(named: "placeholder")!
    static let info: UIImage = UIImage(named: "info")!
    
    func setItemPlaceHolder() {
        self.image = UIImageView.placeHolder
    }
    
    func setInfoImage() {
        self.image = UIImageView.info
    }
}
