//
//  GalleryItem.swift
//  Gallery
//
//  Created by Tony Thomas on 17/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit


struct GalleryItem {
    
    let title: String
    let imageUrl: String
    let image: UIImage?
    
    init(item: GalleryItem, image: UIImage){
        title = item.title
        imageUrl = item.imageUrl
        self.image = image
    }
    
    init(title: String, imageUrl: String, image: UIImage?) {
        self.title = title
        self.imageUrl = imageUrl
        self.image = image
    }
}


