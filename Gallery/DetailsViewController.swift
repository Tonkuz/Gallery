//
//  DetailsViewController.swift
//  Gallery
//
//  Created by Tony Thomas on 18/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.backgroundColor = UIColor.lightGray
        return iv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(imageView)
        doLayout()
        // Do any additional setup after loading the view.
    }

    func doLayout() {
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[iv]-0-|", options: [], metrics: nil, views: ["iv": imageView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[iv]-0-|", options: [], metrics: nil, views: ["iv": imageView]))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
