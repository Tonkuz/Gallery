//
//  ImageDownloader.swift
//  Gallery
//
//  Created by Tony Thomas on 17/5/17.
//  Copyright © 2017 Tony Thomas. All rights reserved.
//

import UIKit

final class ImageDownloader {
    
    let operationQueue = OperationQueue()
    
    private init() {
        operationQueue.maxConcurrentOperationCount = 5
    }
    
    static let shared = ImageDownloader()
    
    func downloadImage(url: String, completion:@escaping (UIImage?) ->Void) {
        
        operationQueue.addOperation {
            if let imgUrl = URL(string: url) {
                
                do {
                    let data = try Data(contentsOf: imgUrl)
                    let image = UIImage(data: data)
                    completion(image)
                } catch {
                    
                }
            }
            completion(nil)
          }
    }
    
    
}
